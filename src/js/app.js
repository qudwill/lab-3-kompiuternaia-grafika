'use strict';


function putPixel(ctx, x, y) {
	ctx.fillRect(x, y, 1, 1);
}

function sleep(milliseconds) {
  var start = new Date().getTime();

  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds) {
      break;
    }
  }
}


/**
 ** Построение окружности по алгоритму Брезенхема 
 */

var Circle = (function() {
	var canvas = document.getElementById('circle-canvas');
	var ctx = canvas.getContext('2d');
	var rValue = 255;
	var xCenter = 0;

	ctx.fillStyle = 'rgb(' + rValue + ', 0, 0)';

	function setupListeners() {
		$('#circle-form').on('submit', _build);
		$('#circle-fill').on('submit', _fill);
	}

	function _build(e) {
		e.preventDefault();

		var $this = $(this);
		var data = $this.serializeArray();
		var x0 = parseInt(data[0].value);
		var y0 = parseInt(data[1].value);
		var radius = parseInt(data[2].value);

		xCenter = x0;

		canvas.style.display = 'block';

		$('#fill-button').removeAttr('disabled');

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		var x = 0;
		var y = radius;
		var delta = 1 - 2 * radius;
		var error = 0;

		while (y >= 0) {
			putPixel(ctx, x0 + x, y0 + y);
			putPixel(ctx, x0 + x, y0 - y);
			putPixel(ctx, x0 - x, y0 + y);
			putPixel(ctx, x0 - x, y0 - y);

			error = 2 * (delta + y) - 1;

			if (delta < 0 && error <= 0) {
				++x;

				delta += 2 * x + 1;

				continue;
			}

			error = 2 * (delta - x) - 1;

			if (delta > 0 && error > 0) {
				--y;

				delta += 1 - 2 * y;

				continue;
			}

			++x;

			delta += 2 * (x - y);

			--y;
		}
	}

	function _fill(e) {
		e.preventDefault();

		var stack = [];
		var $this = $(this);
		var data = $this.serializeArray();
		var x = parseInt(data[0].value);
		var y = parseInt(data[1].value);

		stack.push([x, y]);

		while (stack.length > 0) {
			console.log(stack);

			var coords = stack.pop();
			var x = coords[0];
			var y = coords[1];
			var tempX = x;
			var xBorder = [];
			var xLeft = 0;
			var xRight = 0;

			console.log('Производится закраска линии #' + y);

			for (var i = 0; i < canvas.width; i++) {
				var imageData = ctx.getImageData(i, y, 1, 1).data;

				if (imageData[0] == rValue) {
					xBorder.push(i);
				}

				if (xBorder.length == 2) {
					break;
				}
			}

			while (true) {
				putPixel(ctx, x, y);

				x++;

				if (_isFilled(ctx, x, y)) {
					xRight = x;

					break;
				}
			}

			x = tempX;

			x--;

			while (true) {
				putPixel(ctx, x, y);

				x--;

				if (_isFilled(ctx, x, y)) {
					xLeft = x;

					break;
				}
			}

			var tempY = y;

			y--;

			if (!_isFilled(ctx, xCenter, y)) {
				stack.push([xCenter, y]);
			}

			y = tempY;

			y++;

			if (!_isFilled(ctx, xCenter, y)) {
				stack.push([xCenter, y]);
			}
		}
	}

	function _isFilled(ctx, x, y) {
		var imageData = ctx.getImageData(x, y, 1, 1).data;

		if (imageData[0] == rValue) {
			return true;
		} else {
			return false;
		}
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());

$(document).ready(function() {
	Circle.init();
})